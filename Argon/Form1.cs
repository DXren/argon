﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibDrawingGraphs;

namespace Argon
{
    public partial class Form1 : Form
    {

        Structure str, str_;

   


        private void btn_calculateEnergy_Click(object sender, EventArgs e)
        {

            double energy = str.PotentialEnergy()*Math.Pow(10, 27);
            txt_sumEnergy.Text = energy.ToString("f6");
            txt_atomEnergy.Text = (energy / str.StructNumAtoms).ToString("f6");
        }

        private void button_openToCoord_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenCoordAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        
        private void buttonToNeighborOne_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenFirstNeighboursdAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonbuttonToNeighborTwo_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenSecondNeighboursdAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private FormOptions.ModelingParams modelParams;
        Random RandShift, RandNumGen;
        public Form1()
        {
            InitializeComponent();
        }

        //Метод подсчёта количества атомов структуры в зависимости от её размера
        private void numUD_sizeCells_ValueChanged(object sender, EventArgs e)
        {
            int sizeStruct = (int)numUD_sizeCells.Value;
            int numAtoms = Structure.TotalNumAtoms(sizeStruct);
            txt_numAtoms.Text = numAtoms.ToString();
            double value = Math.Round(numAtoms * 3.0 / 100) * 100;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OutputInfo.Start();
        /*    graphPot = new DrawingGraphs(pict_potEnergy.Width, pict_potEnergy.Height);
            graphKin = new DrawingGraphs(pict_kinEnergy.Width, pict_kinEnergy.Height);
            graphSum = new DrawingGraphs(pict_3.Width, pict_3.Height);
        */
            modelParams = new FormOptions.ModelingParams(0, 0, true);
        }

        // кнопка "Создать"
        private void btn_createStructure_Click(object sender, EventArgs e)
        {
            //Проверяем и очищаем файлы Excel 
            bool success0 = OutputInfo.ClearCoordAtomsFile();
            if (!success0)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            bool success1 = OutputInfo.ClearFirstNeighboursdAtoms();
            if (!success1)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            bool success2 = OutputInfo.ClearSecondNeighboursdAtoms();
            if (!success2)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return; str = new Structure((int)numUD_sizeCells.Value, true, false, false, 0, false);
            int sizeStruct = (int)numUD_sizeCells.Value;
            int numAtoms = Structure.TotalNumAtoms(sizeStruct);
            for (int i = 0; i < numAtoms; i++)
            {
                OutputInfo.WriteCoordAtomFile(i + 1, str.Struct[i].Coordinate.x / str.StructLatPar, str.Struct[i].Coordinate.y / str.StructLatPar, str.Struct[i].Coordinate.z / str.StructLatPar);

                OutputInfo.WriteFirstNeighboursdAtoms(i + 1,
                     str.Struct[i].FirstNeighbours[0].Index + 1, str.Struct[i].FirstNeighbours[0].Coordinate.x / str.StructLatPar, str.Struct[i].FirstNeighbours[0].Coordinate.y / str.StructLatPar, str.Struct[i].FirstNeighbours[0].Coordinate.z / str.StructLatPar,
                     str.Struct[i].FirstNeighbours[1].Index + 1, str.Struct[i].FirstNeighbours[1].Coordinate.x / str.StructLatPar, str.Struct[i].FirstNeighbours[1].Coordinate.y / str.StructLatPar, str.Struct[i].FirstNeighbours[1].Coordinate.z / str.StructLatPar,
                     str.Struct[i].FirstNeighbours[2].Index + 1, str.Struct[i].FirstNeighbours[2].Coordinate.x / str.StructLatPar, str.Struct[i].FirstNeighbours[2].Coordinate.y / str.StructLatPar, str.Struct[i].FirstNeighbours[2].Coordinate.z / str.StructLatPar,
                     str.Struct[i].FirstNeighbours[3].Index + 1, str.Struct[i].FirstNeighbours[3].Coordinate.x / str.StructLatPar, str.Struct[i].FirstNeighbours[3].Coordinate.y / str.StructLatPar, str.Struct[i].FirstNeighbours[3].Coordinate.z / str.StructLatPar);

                OutputInfo.WriteSecondNeighboursdAtoms(i + 1,
                     str.Struct[i].SecondNeighbours[0].Index + 1, str.Struct[i].SecondNeighbours[0].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[0].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[0].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[1].Index + 1, str.Struct[i].SecondNeighbours[1].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[1].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[1].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[2].Index + 1, str.Struct[i].SecondNeighbours[2].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[2].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[2].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[3].Index + 1, str.Struct[i].SecondNeighbours[3].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[3].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[3].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[4].Index + 1, str.Struct[i].SecondNeighbours[4].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[4].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[4].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[5].Index + 1, str.Struct[i].SecondNeighbours[5].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[5].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[5].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[6].Index + 1, str.Struct[i].SecondNeighbours[6].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[6].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[6].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[7].Index + 1, str.Struct[i].SecondNeighbours[7].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[7].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[7].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[8].Index + 1, str.Struct[i].SecondNeighbours[8].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[8].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[8].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[9].Index + 1, str.Struct[i].SecondNeighbours[9].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[9].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[9].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[10].Index + 1, str.Struct[i].SecondNeighbours[10].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[10].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[10].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[11].Index + 1, str.Struct[i].SecondNeighbours[11].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[11].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[11].Coordinate.z / str.StructLatPar);
            }
        }
    }
}
