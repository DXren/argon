﻿
namespace Argon
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.GroupBox groupBox2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.rdBtn_STW = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.grpBox_create = new System.Windows.Forms.GroupBox();
            this.btn_openSecondNeigbords = new System.Windows.Forms.Button();
            this.btn_openFirstNeigborth = new System.Windows.Forms.Button();
            this.btn_openCoordAtoms = new System.Windows.Forms.Button();
            this.txt_latPar = new System.Windows.Forms.TextBox();
            this.txt_numAtoms = new System.Windows.Forms.TextBox();
            this.btn_createStructure = new System.Windows.Forms.Button();
            this.numUD_sizeCells = new System.Windows.Forms.NumericUpDown();
            this.grpBox_energy = new System.Windows.Forms.GroupBox();
            this.txt_atomEnergy = new System.Windows.Forms.TextBox();
            this.txt_sumEnergy = new System.Windows.Forms.TextBox();
            this.btn_calculateEnergy = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button_openToCoord = new System.Windows.Forms.Button();
            this.buttonbuttonToNeighborTwo = new System.Windows.Forms.Button();
            this.buttonToNeighborOne = new System.Windows.Forms.Button();
            label11 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            groupBox2 = new System.Windows.Forms.GroupBox();
            label1 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBox_create.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_sizeCells)).BeginInit();
            this.grpBox_energy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(222, 81);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(25, 17);
            label11.TabIndex = 15;
            label11.Text = "нм";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(6, 50);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(108, 17);
            label8.TabIndex = 22;
            label8.Text = "Кол-во атомов:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Cursor = System.Windows.Forms.Cursors.Help;
            label2.Location = new System.Drawing.Point(6, 25);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(104, 17);
            label2.TabIndex = 4;
            label2.Text = "Размер ячеек:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(6, 83);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(139, 17);
            label4.TabIndex = 16;
            label4.Text = "Параметр решётки:";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(250, 109);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(24, 17);
            label14.TabIndex = 17;
            label14.Text = "эВ";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label13.Location = new System.Drawing.Point(9, 104);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(130, 17);
            label13.TabIndex = 16;
            label13.Text = "Энергия на атом ≈";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(250, 78);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(24, 17);
            label10.TabIndex = 14;
            label10.Text = "эВ";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(this.rdBtn_STW);
            groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            groupBox2.Location = new System.Drawing.Point(6, 19);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new System.Drawing.Size(268, 49);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            groupBox2.Text = "Тип потенциала";
            // 
            // rdBtn_STW
            // 
            this.rdBtn_STW.AutoSize = true;
            this.rdBtn_STW.Checked = true;
            this.rdBtn_STW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdBtn_STW.Location = new System.Drawing.Point(6, 21);
            this.rdBtn_STW.Name = "rdBtn_STW";
            this.rdBtn_STW.Size = new System.Drawing.Size(150, 21);
            this.rdBtn_STW.TabIndex = 2;
            this.rdBtn_STW.TabStop = true;
            this.rdBtn_STW.Text = "Леннарда-Джонса";
            this.rdBtn_STW.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label1.Location = new System.Drawing.Point(9, 78);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(126, 17);
            label1.TabIndex = 2;
            label1.Text = "Энергия системы:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(6, 35);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(192, 17);
            label3.TabIndex = 25;
            label3.Text = "Процент удаляемых атомов";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(label3);
            this.groupBox1.Location = new System.Drawing.Point(22, 190);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(257, 89);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Моделирование вакансии";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.ForeColor = System.Drawing.Color.DarkBlue;
            this.textBox1.Location = new System.Drawing.Point(204, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(47, 22);
            this.textBox1.TabIndex = 26;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "0,54535";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grpBox_create
            // 
            this.grpBox_create.Controls.Add(this.btn_openSecondNeigbords);
            this.grpBox_create.Controls.Add(this.btn_openFirstNeigborth);
            this.grpBox_create.Controls.Add(this.btn_openCoordAtoms);
            this.grpBox_create.Controls.Add(label11);
            this.grpBox_create.Controls.Add(this.txt_latPar);
            this.grpBox_create.Controls.Add(this.txt_numAtoms);
            this.grpBox_create.Controls.Add(label8);
            this.grpBox_create.Controls.Add(this.btn_createStructure);
            this.grpBox_create.Controls.Add(this.numUD_sizeCells);
            this.grpBox_create.Controls.Add(label2);
            this.grpBox_create.Controls.Add(label4);
            this.grpBox_create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_create.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpBox_create.Location = new System.Drawing.Point(22, 12);
            this.grpBox_create.Name = "grpBox_create";
            this.grpBox_create.Size = new System.Drawing.Size(257, 172);
            this.grpBox_create.TabIndex = 3;
            this.grpBox_create.TabStop = false;
            this.grpBox_create.Text = "Параметры создания структуры";
            // 
            // btn_openSecondNeigbords
            // 
            this.btn_openSecondNeigbords.BackColor = System.Drawing.Color.LightCyan;
            this.btn_openSecondNeigbords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_openSecondNeigbords.Location = new System.Drawing.Point(303, 162);
            this.btn_openSecondNeigbords.Name = "btn_openSecondNeigbords";
            this.btn_openSecondNeigbords.Size = new System.Drawing.Size(202, 63);
            this.btn_openSecondNeigbords.TabIndex = 27;
            this.btn_openSecondNeigbords.TabStop = false;
            this.btn_openSecondNeigbords.Text = "Открыть координаты вторых соседей атомов в Excel";
            this.btn_openSecondNeigbords.UseVisualStyleBackColor = false;
            // 
            // btn_openFirstNeigborth
            // 
            this.btn_openFirstNeigborth.BackColor = System.Drawing.Color.LightCyan;
            this.btn_openFirstNeigborth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_openFirstNeigborth.Location = new System.Drawing.Point(303, 81);
            this.btn_openFirstNeigborth.Name = "btn_openFirstNeigborth";
            this.btn_openFirstNeigborth.Size = new System.Drawing.Size(202, 62);
            this.btn_openFirstNeigborth.TabIndex = 26;
            this.btn_openFirstNeigborth.TabStop = false;
            this.btn_openFirstNeigborth.Text = "Открыть координаты первых соседей атомов в Excel";
            this.btn_openFirstNeigborth.UseVisualStyleBackColor = false;
            // 
            // btn_openCoordAtoms
            // 
            this.btn_openCoordAtoms.BackColor = System.Drawing.Color.LightCyan;
            this.btn_openCoordAtoms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_openCoordAtoms.Location = new System.Drawing.Point(303, 16);
            this.btn_openCoordAtoms.Name = "btn_openCoordAtoms";
            this.btn_openCoordAtoms.Size = new System.Drawing.Size(202, 51);
            this.btn_openCoordAtoms.TabIndex = 25;
            this.btn_openCoordAtoms.TabStop = false;
            this.btn_openCoordAtoms.Text = "Открыть  координаты атомов в Excel";
            this.btn_openCoordAtoms.UseVisualStyleBackColor = false;
            // 
            // txt_latPar
            // 
            this.txt_latPar.BackColor = System.Drawing.SystemColors.Control;
            this.txt_latPar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_latPar.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_latPar.Location = new System.Drawing.Point(151, 81);
            this.txt_latPar.Name = "txt_latPar";
            this.txt_latPar.ReadOnly = true;
            this.txt_latPar.Size = new System.Drawing.Size(60, 22);
            this.txt_latPar.TabIndex = 24;
            this.txt_latPar.TabStop = false;
            this.txt_latPar.Text = "0,54535";
            this.txt_latPar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_numAtoms
            // 
            this.txt_numAtoms.BackColor = System.Drawing.SystemColors.Control;
            this.txt_numAtoms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_numAtoms.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_numAtoms.Location = new System.Drawing.Point(119, 49);
            this.txt_numAtoms.Name = "txt_numAtoms";
            this.txt_numAtoms.ReadOnly = true;
            this.txt_numAtoms.Size = new System.Drawing.Size(57, 22);
            this.txt_numAtoms.TabIndex = 23;
            this.txt_numAtoms.TabStop = false;
            this.txt_numAtoms.Text = "64";
            this.txt_numAtoms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_createStructure
            // 
            this.btn_createStructure.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_createStructure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_createStructure.Location = new System.Drawing.Point(0, 116);
            this.btn_createStructure.Name = "btn_createStructure";
            this.btn_createStructure.Size = new System.Drawing.Size(84, 40);
            this.btn_createStructure.TabIndex = 2;
            this.btn_createStructure.Text = "Создать";
            this.btn_createStructure.UseVisualStyleBackColor = false;
            this.btn_createStructure.Click += new System.EventHandler(this.btn_createStructure_Click);
            // 
            // numUD_sizeCells
            // 
            this.numUD_sizeCells.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_sizeCells.Location = new System.Drawing.Point(119, 21);
            this.numUD_sizeCells.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numUD_sizeCells.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUD_sizeCells.Name = "numUD_sizeCells";
            this.numUD_sizeCells.Size = new System.Drawing.Size(42, 22);
            this.numUD_sizeCells.TabIndex = 0;
            this.numUD_sizeCells.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // grpBox_energy
            // 
            this.grpBox_energy.Controls.Add(label14);
            this.grpBox_energy.Controls.Add(this.txt_atomEnergy);
            this.grpBox_energy.Controls.Add(label13);
            this.grpBox_energy.Controls.Add(label10);
            this.grpBox_energy.Controls.Add(groupBox2);
            this.grpBox_energy.Controls.Add(this.txt_sumEnergy);
            this.grpBox_energy.Controls.Add(label1);
            this.grpBox_energy.Controls.Add(this.btn_calculateEnergy);
            this.grpBox_energy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_energy.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpBox_energy.Location = new System.Drawing.Point(285, 12);
            this.grpBox_energy.Name = "grpBox_energy";
            this.grpBox_energy.Size = new System.Drawing.Size(282, 202);
            this.grpBox_energy.TabIndex = 4;
            this.grpBox_energy.TabStop = false;
            this.grpBox_energy.Text = "Параметры рассчёта энергии";
            // 
            // txt_atomEnergy
            // 
            this.txt_atomEnergy.BackColor = System.Drawing.SystemColors.Control;
            this.txt_atomEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_atomEnergy.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_atomEnergy.Location = new System.Drawing.Point(144, 104);
            this.txt_atomEnergy.Name = "txt_atomEnergy";
            this.txt_atomEnergy.ReadOnly = true;
            this.txt_atomEnergy.Size = new System.Drawing.Size(100, 22);
            this.txt_atomEnergy.TabIndex = 15;
            this.txt_atomEnergy.TabStop = false;
            this.txt_atomEnergy.Text = "0";
            this.txt_atomEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_sumEnergy
            // 
            this.txt_sumEnergy.BackColor = System.Drawing.SystemColors.Control;
            this.txt_sumEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_sumEnergy.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_sumEnergy.Location = new System.Drawing.Point(144, 78);
            this.txt_sumEnergy.Name = "txt_sumEnergy";
            this.txt_sumEnergy.ReadOnly = true;
            this.txt_sumEnergy.Size = new System.Drawing.Size(100, 22);
            this.txt_sumEnergy.TabIndex = 1;
            this.txt_sumEnergy.TabStop = false;
            this.txt_sumEnergy.Text = "0";
            this.txt_sumEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_calculateEnergy
            // 
            this.btn_calculateEnergy.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_calculateEnergy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_calculateEnergy.Location = new System.Drawing.Point(12, 142);
            this.btn_calculateEnergy.Name = "btn_calculateEnergy";
            this.btn_calculateEnergy.Size = new System.Drawing.Size(123, 46);
            this.btn_calculateEnergy.TabIndex = 0;
            this.btn_calculateEnergy.Text = "Подсчёт энергии системы";
            this.btn_calculateEnergy.UseVisualStyleBackColor = false;
            this.btn_calculateEnergy.Click += new System.EventHandler(this.btn_calculateEnergy_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(590, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(620, 316);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Location = new System.Drawing.Point(590, 355);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Name = "Series1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(625, 344);
            this.chart1.TabIndex = 6;
            this.chart1.Text = "chart1";
            // 
            // button_openToCoord
            // 
            this.button_openToCoord.BackColor = System.Drawing.Color.PaleTurquoise;
            this.button_openToCoord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_openToCoord.Location = new System.Drawing.Point(12, 302);
            this.button_openToCoord.Name = "button_openToCoord";
            this.button_openToCoord.Size = new System.Drawing.Size(202, 73);
            this.button_openToCoord.TabIndex = 18;
            this.button_openToCoord.Text = "Открыть координаты атомов в Excel";
            this.button_openToCoord.UseVisualStyleBackColor = false;
            this.button_openToCoord.Click += new System.EventHandler(this.button_openToCoord_Click);
            // 
            // buttonbuttonToNeighborTwo
            // 
            this.buttonbuttonToNeighborTwo.BackColor = System.Drawing.Color.PaleTurquoise;
            this.buttonbuttonToNeighborTwo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonbuttonToNeighborTwo.Location = new System.Drawing.Point(12, 473);
            this.buttonbuttonToNeighborTwo.Name = "buttonbuttonToNeighborTwo";
            this.buttonbuttonToNeighborTwo.Size = new System.Drawing.Size(202, 63);
            this.buttonbuttonToNeighborTwo.TabIndex = 29;
            this.buttonbuttonToNeighborTwo.TabStop = false;
            this.buttonbuttonToNeighborTwo.Text = "Открыть координаты вторых соседей атомов в Excel";
            this.buttonbuttonToNeighborTwo.UseVisualStyleBackColor = false;
            this.buttonbuttonToNeighborTwo.Click += new System.EventHandler(this.buttonbuttonToNeighborTwo_Click);
            // 
            // buttonToNeighborOne
            // 
            this.buttonToNeighborOne.BackColor = System.Drawing.Color.PaleTurquoise;
            this.buttonToNeighborOne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonToNeighborOne.Location = new System.Drawing.Point(12, 392);
            this.buttonToNeighborOne.Name = "buttonToNeighborOne";
            this.buttonToNeighborOne.Size = new System.Drawing.Size(202, 62);
            this.buttonToNeighborOne.TabIndex = 28;
            this.buttonToNeighborOne.TabStop = false;
            this.buttonToNeighborOne.Text = "Открыть координаты первых соседей атомов в Excel";
            this.buttonToNeighborOne.UseVisualStyleBackColor = false;
            this.buttonToNeighborOne.Click += new System.EventHandler(this.buttonToNeighborOne_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 723);
            this.Controls.Add(this.buttonbuttonToNeighborTwo);
            this.Controls.Add(this.buttonToNeighborOne);
            this.Controls.Add(this.button_openToCoord);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.grpBox_energy);
            this.Controls.Add(this.grpBox_create);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpBox_create.ResumeLayout(false);
            this.grpBox_create.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_sizeCells)).EndInit();
            this.grpBox_energy.ResumeLayout(false);
            this.grpBox_energy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpBox_create;
        private System.Windows.Forms.Button btn_openSecondNeigbords;
        private System.Windows.Forms.Button btn_openFirstNeigborth;
        private System.Windows.Forms.Button btn_openCoordAtoms;
        private System.Windows.Forms.TextBox txt_latPar;
        private System.Windows.Forms.TextBox txt_numAtoms;
        private System.Windows.Forms.Button btn_createStructure;
        private System.Windows.Forms.NumericUpDown numUD_sizeCells;
        private System.Windows.Forms.GroupBox grpBox_energy;
        private System.Windows.Forms.TextBox txt_atomEnergy;
        private System.Windows.Forms.RadioButton rdBtn_STW;
        private System.Windows.Forms.TextBox txt_sumEnergy;
        private System.Windows.Forms.Button btn_calculateEnergy;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button_openToCoord;
        private System.Windows.Forms.Button buttonbuttonToNeighborTwo;
        private System.Windows.Forms.Button buttonToNeighborOne;
    }
}

